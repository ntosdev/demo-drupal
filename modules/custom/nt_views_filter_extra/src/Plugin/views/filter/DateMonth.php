<?php

namespace Drupal\nt_views_filter_extra\Plugin\views\filter;

use Drupal\Core\Datetime\DateHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\views\filter\Date;

/**
 * Date/time views filter.
 *
 * Even thought dates are stored as strings, the numeric filter is extended
 * because it provides more sensible operators.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("datetime_month")
 */
class DateMonth extends Date {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, \Drupal\Core\Datetime\DateFormatterInterface $date_formatter, \Symfony\Component\HttpFoundation\RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $date_formatter, $request_stack);
    $this->dateFormat = 'm';
  }

  public function operators() {
    $operators = parent::operators();

    // Remove complex operators.
    foreach ($operators as $operator => $definition) {
      if ('opSimple' != $definition['method']) {
        unset($operators[$operator]);
      }
    }

    return $operators;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    $exposed = $form_state->get('exposed');
    $options = DateHelper::monthNames(TRUE);

    if ($exposed) {
      unset($form['value']['#size']);
      $form['value']['#type'] = 'select';
      $form['value']['#options'] = $options;
    }
    else {
      $options = ['All' => $this->t('- Any -')] + $options;
      unset($form['value']['value']['#size']);
      $form['value']['value']['#type'] = 'select';
      $form['value']['value']['#options'] = $options;

      // Remove unnecessary fields.
      unset($form['value']['type']);
      unset($form['value']['min']);
      unset($form['value']['max']);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple($field) {
    if (empty($this->value) || 'All' == $this->value['value']) {
      return;
    }

    $placeholder = $this->placeholder();
    $format = $this->getDateFormat($this->dateFormat);

    $placeholders = [
      $placeholder => str_pad($this->value['value'], 2, 0, STR_PAD_LEFT),
    ];

    $this->query->addWhere($this->options['group'], "$format $this->operator $placeholder", $placeholders, 'formula');
  }

  /**
   * {@inheritdoc}
   */
  public function getDateField() {
    // Use string date storage/formatting since datetime fields are stored as
    // strings rather than UNIX timestamps.
    return $this->query->getDateField("$this->tableAlias.$this->realField", TRUE, $this->calculateOffset);
  }

  /**
   * {@inheritdoc}
   */
  public function getDateFormat($format) {
    // Pass in the string-field option.
    return $this->query->getDateFormat($this->getDateField(), $format, TRUE);
  }

}


