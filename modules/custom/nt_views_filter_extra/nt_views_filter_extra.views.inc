<?php

/**
 * @file
 * Provide views data for npi_seminars.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_field_views_data_alter().
 */
function nt_views_filter_extra_field_views_data_alter(array &$data, \Drupal\field\FieldStorageConfigInterface $field_storage) {
  if ('datetime' == $field_storage->getType()) {
    $field_name = $field_storage->getName();

    foreach ($data as $table_name => $table_data) {
      if (isset($data[$table_name][$field_name . '_value']) && isset($data[$table_name][$field_name . '_value_month'])) {
        $data[$table_name][$field_name . '_value_month']['filter'] = $data[$table_name][$field_name . '_value']['filter'];
        $data[$table_name][$field_name . '_value_month']['filter']['id'] = 'datetime_month';
      }
      if (isset($data[$table_name][$field_name . '_value']) && isset($data[$table_name][$field_name . '_value_year'])) {
        $data[$table_name][$field_name . '_value_year']['filter'] = $data[$table_name][$field_name . '_value']['filter'];
        $data[$table_name][$field_name . '_value_year']['filter']['id'] = 'datetime_year';
      }
    }
  }
}