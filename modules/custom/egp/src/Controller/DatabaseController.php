<?php

namespace Drupal\egp\Controller;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class DatabaseController {
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Inserts into the database specific table and data
   *
   * @param string $table
   * @param array $entry
   *
   * @return int
   *   The number of inserted rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert($table, array $entry) {
    $return_value = 0;
    try {
      $return_value = $this->connection->insert($table)
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage(t('Failed to insert the record. Message = %message', [
        '%message' => $e->getMessage(),
      ]), 'error');
    }
    return $return_value;
  }

  /**
   * Updates specific record in the table provided
   *
   * @param string $table
   * @param array $entry
   * @param array $conditionals
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database update fails.
   */
  public function update($table, array $entry, array $conditionals) {
    $count = 0;
    try {
      $count = $this->connection->update($table)
        ->fields($entry);

      // Add each field and value as a condition to this query.
      foreach ($conditionals as $value) {
        $count->condition($value['field'], $value['value'], $value['operator']);
      }

      $count->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage(t('Failed to update the record. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]
      ), 'error');
    }
    return $count;
  }

  /**
   * Deletes specific record in the table provided
   *
   * @param string $table
   * @param array $conditionals
   *
   * @return int
   *   The number of deleted rows.
   *
   * @throws \Exception
   *   When the database delete fails.
   */
  public function delete($table, array $conditionals) {
    $count = 0;
    try {
      $count = $this->connection->delete($table);

      // Add each field and value as a condition to this query.
      foreach ($conditionals as $value) {
        $count->condition($value['field'], $value['value'], $value['operator']);
      }

      $count->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage(t('Failed to delete the record. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]
      ), 'error');
    }
    return $count;
  }

  /**
   * Loads values from the table provided
   *
   * @param string $table
   * @param array $entry
   *
   * @return mixed
   *
   * @throws \Exception
   *   When the database delete fails.
   */
  public function load($table, array $entry = []) {
    $count = 0;
    try {
      // Read all the fields
      $select = $this->connection
        ->select($table)
        // Add all the fields into our select query.
        ->fields($table);

      // Add each field and value as a condition to this query.
      foreach ($entry as $field => $value) {
        $select->condition($field, $value);
      }
      // Return the result in object format.
      return $select->execute()->fetchAll();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage(t('Unable to retrieve records from the the database. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]
      ), 'error');
    }
    return $count;
  }

  /**
   * Loads values from the table provided
   *
   * @param string $table
   * @param array $entry
   * @param array $extra
   *
   * @return mixed
   *
   * @throws \Exception
   *   When the database delete fails.
   */
  public function advancedLoad($table, array $entry = [], array $extra = []) {
    $count = 0;
    try {
      // Read all the fields
      $select = $this->connection
        ->select($table)
        // Add all the fields into our select query.
        ->fields($table);

      // Add each field and value as a condition to this query.
      foreach ($entry as $field => $value) {
        $select->condition($field, $value);
      }

      foreach($extra as $type => $values) {
        switch ($type) {
          case 'inner_join':
            $select->innerJoin($values['table'], $values['table'], $values['condition']);
            break;
          case 'order':
            $select->orderBy($values['field'], $values['direction']);
            break;
          case 'limit':
            $select->range($values['start'], $values['length']);
            break;
          default:
            break;
        }
      }

      // Return the result in object format.
      return $select->execute()->fetchAll();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage(t('Unable to retrieve records from the the database. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]
      ), 'error');
    }
    return $count;
  }
}
