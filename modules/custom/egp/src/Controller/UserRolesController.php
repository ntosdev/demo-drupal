<?php

namespace Drupal\egp\Controller;

use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserRolesController {

  /**
   * The repository for our specialized queries.
   *
   * @var \Drupal\nt\Controller\DatabaseController
   */
  protected $repository;

  static $table = 'granular_permissions';
  static $permissions = 'administer paragraphs types';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $controller = new static($container->get('egp.repository'));
    return $controller;
  }

  /**
   * Construct a new controller.
   *
   * @param \Drupal\egp\Controller\DatabaseController $repository
   *   The repository service.
   */
  public function __construct(DatabaseController $repository) {
    $this->repository = $repository;
  }

  public function listDefaultPermissions() {
    $default_permissions = [];
    $entities = [];
    foreach (user_roles() as $rid => $role) {
      if($role->id() != 'administrator') {
        $default_permissions[$role->id()] = [
          'label' => $role->label(),
        ];
        foreach($role->getPermissions() as $permission) {
          if(strpos($permission, 'view paragraph content') !== FALSE) {
            $entity = explode(' ', $permission);
            $entities[] = $entity[3];
            $default_permissions[$role->id()]['permissions'][$entity[3]] = 1;
          }
        }
      }
    }

    // We verify if "Anonymous" and "Authenticated" is True, because other
    // roles will come back as False if these 2 are checked.
    foreach($default_permissions as $role => $permission) {
      if($role != 'anonymous' && $role != 'authenticated') {
        foreach($entities as $entity) {
          if(
            isset($default_permissions['anonymous']['permissions'][$entity]) &&
            isset($default_permissions['authenticated']['permissions'][$entity])
          ) {
            $default_permissions[$role]['permissions'][$entity] = 1;
          }
        }
      }
    }
    return $default_permissions;
  }

  public function listGranularPermissions($nid, $pid, $role, $type, $value = []) {
    $results = $this->repository->load(
      self::$table,
      [
        'nid' => $nid,
        'pid' => $pid,
        'role' => $role,
      ]
    );

    if(count($results) > 0) {
      foreach($results as $record) {
        $value['permissions'][$type] = $record->access;
      }
    }

    return $value;
  }

  public function entityPermission($entity) {
    $paragraph = [];
    $user = \Drupal::currentUser();

    $paragraph['nid'] = $entity['paragraph']->getParentEntity()->id();
    $paragraph['pid'] = $entity['paragraph']->id();
    $paragraph['type'] = $entity['paragraph']->getType();
    $paragraph['user'] = $user;
    $paragraph['roles'] = $user->getRoles();
    $paragraph['edit'] = $user->hasPermission(self::$permissions);
    $paragraph['access'] = FALSE;
    $paragraph['exists'] = FALSE;

    foreach($paragraph['roles'] as $role) {
      $results = $this->repository->load(
        self::$table,
        [
          'nid' => $paragraph['nid'],
          'pid' => $paragraph['pid'],
          'role' => $role,
        ]
      );

      if(count($results) > 0) {
        $paragraph['exists'] = TRUE;
        foreach($results as $record) {
          if($record->access == 1) {
            $paragraph['access'] = TRUE;
          }
        }
      } else {
        $paragraph['access'] = TRUE;
      }
    }

    $paragraph['url'] = Url::fromRoute(
      ($paragraph['exists']) ? 'egp.update' : 'egp.add',
      [
        'pid' => $paragraph['pid'],
        'nid' => $paragraph['nid'],
        'type' => $paragraph['type']
      ],
      [
        'query' => \Drupal::service('redirect.destination')->getAsArray()
      ]
    );

    return $paragraph;
  }

}
