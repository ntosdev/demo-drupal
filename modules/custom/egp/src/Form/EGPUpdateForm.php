<?php

namespace Drupal\egp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\egp\Controller\DatabaseController;
use Drupal\egp\Controller\UserRolesController;

/**
 * Update a record Form.
 *
 * @ingroup egp
 */
class EGPUpdateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'egp_update_form';
  }

  /**
   * Updates Permission Record
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $nid = $_GET['nid'];
    $pid = $_GET['pid'];
    $type = $_GET['type'];

    $user = new UserRolesController(new DatabaseController(\Drupal::database()));
    $roles = $user->listDefaultPermissions();
    foreach($roles as $role => $value) {
      $value = $user->listGranularPermissions($nid, $pid, $role, $type, $value);
      $form['role_' . $role . '-' . $nid . '-' . $pid] = [
        '#type' => 'checkbox',
        '#title' => $this->t($value['label']),
        '#default_value' => !empty($value['permissions'][$type]) ? TRUE : '',
        '#attributes' =>
          [
            'class' => ['permissions-inline']
          ]
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update Permissions'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Verify that the user is logged-in.
    if ($this->currentUser()->isAnonymous()) {
      $form_state->setError($form, $this->t('You must be logged in to update values to the database.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $repository = new DatabaseController(\Drupal::database());
    // Gather the current user so the new record has ownership.
    $account = $this->currentUser();
    // Save the submitted entry.
    $return = 0;
    $entry = [];
    $values = $form_state->getValues();
    $new_permissions = [];

    foreach($values as $key => $value) {
      if(strpos($key, 'role_') !== FALSE) {
        $items = explode('-', $key);
        $new_permissions[$key] = [
          'role' => str_replace('role_', '', $items[0]),
          'nid' => $items[1],
          'pid' => $items[2],
          'access' => $value
        ];
      }
    }

    foreach($new_permissions as $new_permission) {
      $entry = [
        'pid' => $new_permission['pid'],
        'nid' => $new_permission['nid'],
        'role' => $new_permission['role'],
        'access' => $new_permission['access'],
        'updated_by' => $account->id(),
        'updated_on' => (int) time(),
      ];

      $return = $repository->update(
        'granular_permissions',
        $entry,
        [
          [
            'field' => 'pid',
            'value' => $new_permission['pid'],
            'operator' => '=',
          ],
          [
            'field' => 'nid',
            'value' => $new_permission['nid'],
            'operator' => '=',
          ],
          [
            'field' => 'role',
            'value' => $new_permission['role'],
            'operator' => '=',
          ],
        ]
      );
    }

    if ($return) {
      $this->messenger()->addMessage($this->t('Paragraphs Permission successfully updated.'));
    }
    drupal_flush_all_caches();
  }

}
