(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.global = {
    attach: function(context, settings) {

    }
  };

  $(document).ready(function() {
    $("#block-d8-search").append('<div class="search-close">&times;</div>');

    $(".site-search").click(function(e){
      e.preventDefault();
      $("#block-d8-search").toggleClass('open');
    });

    $(".search-close").click(function(e){
      e.preventDefault();
      $("#block-d8-search").toggleClass('open');
    });
  });

}(jQuery, Drupal));
