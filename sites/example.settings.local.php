<?php

$settings['install_profile'] = 'standard';

/* Disabling All Caches For Development */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['extension_discovery_scan_tests'] = FALSE;
$settings['twig_debug'] = TRUE;

/* Trusted Host Patterns */
$settings['trusted_host_patterns'] = array(
  '^.+\.docker\.localhost$',
  '^.+\.newtargetops\.net$',
  '^.+\.newtarget\.net$',
  '^.+\.newtarget\.net$',
  '^.+\.dd$',
);

/**
 * Localhost Server Configurations
 *
 * Example for Aquia Dev Desktop
 * if (isset($_SERVER['DEVDESKTOP_DRUPAL_SETTINGS_DIR']) && file_exists($_SERVER['DEVDESKTOP_DRUPAL_SETTINGS_DIR'] . '/loc_base_drupal8_dd.inc')) {
 *  require $_SERVER['DEVDESKTOP_DRUPAL_SETTINGS_DIR'] . '/loc_base_drupal8_dd.inc';
 * }
 *
 * Example for LAMPP Stacks
 * $databases['default']['default'] = array (
 *   'database' => 'databasename',
 *   'username' => 'sqlusername',
 *   'password' => 'sqlpassword',
 *   'host' => 'localhost',
 *   'port' => '3306',
 *   'driver' => 'mysql',
 *   'prefix' => '',
 *   'collation' => 'utf8mb4_general_ci',
 * );
 *
 */
