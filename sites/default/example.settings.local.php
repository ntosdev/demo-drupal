<?php

$settings['install_profile'] = 'standard';

/* Disabling All Caches For Development */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['extension_discovery_scan_tests'] = FALSE;
$settings['twig_debug'] = TRUE;

/* Trusted Host Patterns */
$settings['trusted_host_patterns'] = array(
  '^.+\.docker\.localhost$',
  '^.+\.newtargetops\.net$',
  '^.+\.newtarget\.net$',
  '^.+\.newtarget\.net$',
  '^.+\.dd$',
);

// Database Configuration for the local environment
$databases['default']['default'] = array (
  'database' => 'app_database',
  'username' => 'root',
  'password' => 'databasepass',
  'prefix' => '',
  'host' => 'database_mariadb_server',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['install_profile'] = 'standard';
$config_directories['sync'] = 'sites/default/files/config_ILtYdrL8mdr_40C69yGbp_Q4S_MM2rxgKAINWoqkwED8Nci0qhADYhcdVPPoiGgMGqEyh5jP5g/sync';
